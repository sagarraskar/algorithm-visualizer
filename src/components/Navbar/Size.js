import { useState, useEffect } from "react";

export default function Size(props) {
    const [size, setSize] = useState(10);
    const [error, setError] = useState(false);
    const [errmsg, setErrMsg] = useState("");
    useEffect(() => {
        props.onChange(size);
    }, [])
    const handleKeyDown = (e) => {
        if (e.key === 'Enter'){
            console.log(Number.isInteger(size));
            if (Number.isInteger(size) && size >= 10 && size <= 50){
                setError(false);
                props.onChange(size);
            }
            else if(!Number.isInteger(size) && size != 0){
                setError(true);
                setErrMsg("*should be integer");
            }   
            else {
                setError(true);
                setErrMsg("*between 10-50");
            }
        }
    }

    const handleChange = (e) => {
        setSize(e.target.value != 0 ? Number(e.target.value): e.target.value);
    }

    const classNames = error ? "invalid" : "valid";
    return (
        <div className="select-size">
            {/* <select 
                className="select-size"
                name="size" 
                onChange = {(e) => props.onChange(e.target.value)}
                title="select array size"
            >   
                {props.sizes.map((element, index) => 
                    <option key = {element} value = {element}>
                        {element}
                    </option>
                )}
            </select> */}
            <input type="number" className={classNames} onKeyDown={handleKeyDown} onChange={handleChange} value={size}></input>
            {error ? <span className="error">{errmsg}</span> : null}
        </div>
    );
}