import { newSteps, addStep } from "./steps"

export const InsertionSortDesc = {
    name: "Insertion Sort", 
    algorithm:(
        <div>
            <pre>for (i = 0 to indexofLastElement) </pre>
            <pre>   value = array[i]</pre>
            <pre>   j = i</pre>
            <pre>   while(j > 0  &amp;&amp; value &lsaquo; array[j-1])</pre>
            <pre>       array[j] = array[j-1]</pre>
            <pre>       j = j - 1</pre>
            <pre>   array[j] = value</pre>
        </div>        
    ),
    performance: {
        worstCase: (<span>O(n<sup>2</sup>)</span>),
        avgCase: (<span>O(n<sup>2</sup>)</span>),
        bestCase: (<span>O(n)</span>),
        spaceComplexity: (<span>O(n)</span>)
    }

}

export default function InsertionSort(array, length) {
    const steps = newSteps(array);

    let instruction = "";
    for(let i = 1; i < length;i++) {
        let value = array[i];
        let j = i;
        let index = 0;
        instruction = `selecting value ${array[j]} for comparison`;
        addStep(steps, array, steps[steps.length - 1].sorted, instruction, [i]);
        while(j > 0 && value < array[j-1]) {
            instruction = `${array[j-1]} > ${value} is true, hence move ${array[j-1]} to the right by 1`
            index = steps[steps.length-1].sorted.indexOf(j-1);
            index == -1 ? null : steps[steps.length-1].sorted[index] = j;
            addStep(steps, array, steps[steps.length - 1].sorted, instruction, [j], [j-1])
            array[j] = array[j-1];
            j = j-1;
            instruction = `Figure out where to insert ${value} by comparing with element ${array[j-1]}`
            addStep(steps, array, steps[steps.length-1].sorted, instruction, [], [j, j+1])
        }
        instruction = `${array[j-1]} > ${value} is false so insert the element ${value} here`
        addStep(steps, array, [...steps[steps.length-1].sorted, j], instruction, [], [], [j]);
        array[j] = value;

    }

    addStep(steps, array, [...Array(array.length).keys()]);
    return steps;
}