export default function Algorithms(props) {
    return (
        <>
            <select 
                className="select-algo" name="Algorithm" 
                onChange = {(e) => props.onChange(e.target.value)}
                title="select algrithm"
            >
                {props.algorithms.map((element, index) => 
                    <option key = {index} value = {element}>
                        {element}
                    </option>
                )}
            </select>
        </>
    );
}