import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlay, faPause, faRedo } from '@fortawesome/free-solid-svg-icons';
export default function Controls(props) {
    return(
        <div className="Controls">
            <button className="repeat" onClick={() => props.repeat()} title="repeat">
                <FontAwesomeIcon icon={faRedo}/>
            </button>
            <button className={"playbtn" + (props.playDisabled ? " disabled" : "")} onClick={props.running ? props.onPause : props.onPlay} disabled={props.playDisabled} title="play/pause">
              {props.running ? <FontAwesomeIcon icon={faPause}/> : <FontAwesomeIcon icon={faPlay}/>}
            </button>
            <select id="speed" onChange={(e) => props.onSpeedChange(e.target.value)} title="change speed">
                <option value="0.25">0.25x</option>
                <option value="0.5">0.5x</option>
                <option value="1">1x</option>
                <option value="2">2x</option>
                <option value="4">4x</option>
            </select>
        </div>   
    );
}