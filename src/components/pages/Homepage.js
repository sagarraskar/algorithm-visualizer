import { useState, useEffect } from 'react';
import Navbar from "../Navbar/Navbar";
import Visualizer from "../Visualizer/Visualizer";
import randomArray from '../Array';

// algorithms
import BubbleSort, { BubbleSortDesc } from '../Algorithms/BubbleSort';
import InsertionSort, { InsertionSortDesc } from '../Algorithms/InsertionSort';
import SelectionSort, { SelectionSortDesc } from '../Algorithms/SelectionSort';
import MergeSort, { MergeSortDesc } from '../Algorithms/MergeSort';
import QuickSort, { QuickSortDesc } from '../Algorithms/QuickSort';

export default function Home() {
    const [array, setArray] = useState([]);
    const [size, setSize] = useState(10);
    const [steps, setSteps] = useState([]);
    const [algorithm, setAlgorithm] = useState("Bubble Sort");
    

    const algorithms = {
        "Bubble Sort"   : BubbleSort,
        "Insertion Sort": InsertionSort,
        "Selection Sort": SelectionSort,
        "Merge Sort"    : MergeSort,
        "Quick Sort"    : QuickSort 
    };

    const desc = {
        "Bubble Sort"   : BubbleSortDesc,
        "Insertion Sort": InsertionSortDesc,
        "Selection Sort": SelectionSortDesc,
        "Merge Sort"    : MergeSortDesc,
        "Quick Sort"    : QuickSortDesc
    }

    useEffect(() => {
        generateArray();
    }, []);

    useEffect(() => {
        generateArray();
    }, [algorithm, size]);

    useEffect(() => {
        getSteps();
    }, [array]);

    const getSteps = () => {
        const numbers = [...array];
        const algorithm_fun = algorithms[algorithm];
        if (algorithm_fun) {
            const temp_steps = algorithm_fun(numbers, size);
            setSteps(temp_steps);
        } 
    }

    const generateArray = () => {
        setArray(randomArray(size));
        setSteps([]);
    }

    const onAlgorithmChange = (value) => {
        setAlgorithm(value);
    }

    const onSizeChange = (value) => {
        setSize(Number(value));
        // generateArray();
    }

    return (
        <div>
            <Navbar
                onAlgorithmChange={onAlgorithmChange}
                onSizeChange={onSizeChange}
                generateRandomArray={generateArray}
            >

            </Navbar>
            <Visualizer
                array={array}
                steps={steps}
                desc={desc[algorithm]}
            />
        </div>
    )
}