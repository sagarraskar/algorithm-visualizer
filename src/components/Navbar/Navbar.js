import { useState } from 'react';
import Algorithms from './Algorithms';
import Size from './Size';


export default function Navbar(props) {

    return (
        <div className="navbar">
            <div className="nav-brand">
                <a href="/">Algorithm Visualizer</a>
            </div>
            <div className="nav-items center">
                <div className="nav-item">
                    <Algorithms
                        onChange={props.onAlgorithmChange}
                        algorithms={[
                            "Bubble Sort",
                            "Insertion Sort",
                            "Selection Sort",
                            "Merge Sort",
                            "Quick Sort"
                        ]}
                    />
                </div>
                <div className="nav-item">
                    <Size
                        onChange={props.onSizeChange}
                    />
                </div>

                <div className="nav-item">
                    <button id="random" className="btn" onClick={() => props.generateRandomArray()} title="generate random array">Random</button>
                </div>
            </div>
            <div className="nav-items end">
                <div className="nav-item help-container">
                    <a id="help" href="/help">Help</a>
                </div>
            </div>
        </div>
    );
}