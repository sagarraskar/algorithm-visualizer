export default function Performance(props) {
    return (
        <div className="performance-container">
            <div className="performance">
                <h2>Performance</h2>
                <pre>Worst Case Time Complexity: {props.performance.worstCase}</pre>
                <pre>Average Case Time Complexity: {props.performance.avgCase}</pre>
                <pre>Best Case Time Complexity: {props.performance.bestCase}</pre>
                <pre>Space Complexity: {props.performance.spaceComplexity}</pre>
            </div>
        </div>
    );
}