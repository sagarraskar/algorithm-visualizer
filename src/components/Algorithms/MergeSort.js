import { newSteps, addStep } from './/steps';

export const MergeSortDesc =  {
    name: "Merge Sort",
    algorithm: (
        <div>
            <p><b>mergeSort :</b></p>
            <pre>mergeSort(arr, start, end) </pre>
            <pre>   mid = (end-start) / 2</pre>
            <pre>   call mergeSort(arr, start, mid)</pre>
            <pre>   call mergeSort(arr, mid, end)</pre>
            <pre>   call merge(arr, start, mid, end)</pre>
            <pre>      </pre>
            <p><b>merge:</b></p>
            <pre>merge(arr, start, mid, end) </pre>
            <pre>   leftarr = arr[start to mid]</pre>
            <pre>   rightarr = arr[mid to end]</pre>
            <pre>   i = j = k = 0</pre>
            <pre>   while (i &lt; (mid-start) and j &lt; (end-mid)) </pre>
            <pre>       if (leftarr[i] &lt;= rightarr[j])</pre>
            <pre>           arr[k] = leftarr[i]</pre>
            <pre>           i = i + 1</pre>
            <pre>           k = k + 1</pre>
            <pre>       else </pre>
            <pre>           arr[k] = rightarr[j]</pre>
            <pre>           j = j + 1</pre>
            <pre>           k = k + 1</pre>
            <pre>   copy remaining elements to original array</pre>
            <pre>    </pre>
        </div>
    ),
    performance: {
        worstCase: (<span>O(nlog(n))</span>),
        avgCase: (<span>O(nlog(n))</span>),
        bestCase: (<span>O(nlog(n))</span>),
        spaceComplexity: (<span>O(n)</span>)
    }
}


export default function MergeSort (array, length){
    const steps = newSteps(array);
    let instruction = "";
    function merge(arr, start, mid, end) {
        const leftarr = arr.slice(start, mid);
        const rightarr = arr.slice(mid, end);
        
        let i,j,k;
        i = j = k = 0;
        while(i < leftarr.length && j < rightarr.length) {
            if (leftarr[i] <= rightarr[j]) {
                instruction = `left array is : [${leftarr}] and rigt array is: [${rightarr}], ` + `${leftarr[i]} <= ${rightarr[j]} so copy ${leftarr[i]} here`;
                addStep(steps, arr, [], instruction, [], [], [k+start]);
                arr[k+start] = leftarr[i];
                i++;
                instruction = ``;
                addStep(steps, arr, [], instruction, [], [], [k+start]);
            }
            else {
                instruction = `left array is : [${leftarr}] and rigt array is: [${rightarr}], ` + `${leftarr[i]} > ${rightarr[j]}  so copy ${rightarr[j]} here`;
                addStep(steps, arr, [], instruction, [], [], [k+start]);
                arr[k+start] = rightarr[j];
                j++;
                instruction = ``;
                addStep(steps, arr, [], instruction, [], [], [k+start]);
            }
            k++;
        }

        while(i < leftarr.length) {
            instruction = `Copy the remaining elements of left array to original array`;
            addStep(steps, arr, [], instruction, [], [], [k+start]);
            arr[k+start] = leftarr[i];
            i++;
            k++;
            instruction = ``;
            addStep(steps, arr, [], instruction, [], [], [k+start-1]);
        }

        while(j < rightarr.length) {
            instruction = `Copy the remaining elements of right array to original array`;
            addStep(steps, arr, [], instruction, [], [], [k+start]);
            arr[k+start] = rightarr[j];
            j++;
            k++;
            instruction = ``;
            addStep(steps, arr, [], instruction, [], [], [k+start-1]);
        }

        leftarr.length = 0;
        rightarr.length = 0;
    }

    function mergeSort(arr, start, end) {
        let length = end - start;
        if (length == 0) {
            return arr;
        }
        if (length == 1) {
            instruction = `It is single element so it is already sorted`;
            addStep(steps, arr, [start], instruction);
            return [arr[start]];
        }

        const mid = Math.floor((start + end)/2);
        
        instruction = `Calling mergeSort for these elements`;
        addStep(steps, arr, [], instruction, [...Array(mid-start).keys()].map((i) => i + start));
        mergeSort(arr, start, mid);
        
        instruction = `Left array is sorted now`;
        addStep(steps, arr, [...Array(mid-start).keys()].map((i) => start+i), instruction);
       
        instruction = `Calling mergeSort for these elements`;
        addStep(steps, arr, [], instruction, [...Array(end-mid).keys()].map((i) => mid + i));
        mergeSort(arr, mid, end);
        
        instruction = `Right array is sorted now`;
        addStep(steps, arr, [...Array(end-mid).keys()].map((i) => mid + i), instruction);
        
        instruction = `Calling merge for these elements`;
        addStep(steps, arr, [], instruction, [...Array(end-start).keys()].map((i) => start + i), [], []);
        merge(arr, start, mid, end);
    }

    mergeSort(array, 0, length);

    instruction = `Array is sorted`
    addStep(steps, array, [...Array(length).keys()], instruction);
    return steps;
};