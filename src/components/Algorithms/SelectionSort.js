import { newSteps, addStep } from './steps';

export const SelectionSortDesc =  {
    name: "Selection Sort",
    algorithm: (
        <div>
            <pre>for (i = 0 to indexofLastElement-1)</pre>
            <pre>   minIndex = i</pre>
            <pre>   for (j = i+1 to indexofLastUnsortedElement)</pre>
            <pre>       if (array[j] &lt; array[minIndex])</pre>
            <pre>           minIndex = j</pre>
            <pre>   swap(array[i], array[minIndex])</pre>
        </div>
    ),
    performance: {
        worstCase: (<span>O(n<sup>2</sup>)</span>),
        avgCase: (<span>O(n<sup>2</sup>)</span>),
        bestCase: (<span>O(n<sup>2</sup>)</span>),
        spaceComplexity: (<span>O(1)</span>)
    }
}


export default function SelectionSort (array, length){
    let steps = newSteps(array);
    let temp;
    let instruction = ""
    for(let i = 0; i < length-1;i++) {
        let minIndex = i;
        instruction=`${array[i]} is selected as min element`;
        addStep(steps, array, steps[steps.length-1].sorted, instruction, [i]);
        for(let j = i+1;j < length; j++) {
            instruction = `Comparing ${array[j]} and ${array[minIndex]}`
            addStep(steps, array, steps[steps.length - 1].sorted, instruction, [minIndex, j]);
            if(array[j] < array[minIndex]){
                instruction=`${array[j]} < ${array[minIndex]} is true so assign ${j} to minIndex`;
                addStep(steps, array, steps[steps.length-1].sorted, instruction, [minIndex], [j]);
                minIndex = j;
                instruction = `${array[j]} is now min element`
                addStep(steps, array, steps[steps.length - 1].sorted, instruction, [j]);
            }
        }
        instruction=`Swap ${array[i]} with min value ${array[minIndex]}`;
        addStep(steps, array, steps[steps.length-1].sorted, instruction, [], [i, minIndex]);
        temp = array[i];
        array[i] = array[minIndex];
        array[minIndex] = temp;
        instruction=`Now ${array[i]} is sorted`;
        addStep(steps, array, [...steps[steps.length-1].sorted, i], instruction, [], []);
       
    }
    instruction=`Array is sorted`;
    addStep(steps, array, [...steps[steps.length-1].sorted, length-1], instruction);
    return steps;
};