export default function Algorithm(props) {
    return (
        <div className="algorithm-container">
            <div className="algorithm">
                <h1>{props.name}</h1>
                {props.algorithm}
            </div>
        </div>
    );
}