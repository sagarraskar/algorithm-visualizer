import { newSteps, addStep } from './/steps';

export const QuickSortDesc =  {
    name: "Quick Sort",
    algorithm: (
        <div>
            <p><b>Partition</b></p>
            <pre>partition (arr, start, end):</pre>
            <pre>   i = start + 1; </pre>
            <pre>   j = start + 1</pre>
            <pre>   while (j &lt;= end)</pre>
            <pre>       if (arr[j] &lt; arr[start])</pre>
            <pre>           swap(arr[j], arr[start])</pre>
            <pre>           i = i+1</pre>
            <pre>       j = j+1</pre>
            <pre>   swap(arrs[start], arr[i-1])</pre>
            <pre>   return i - 1;</pre>
            <pre>   </pre>
            <p><b>quickSort(arr, start, end):</b></p>
            <pre>   if (start >= end)</pre>
            <pre>       return</pre>
            <pre>   pivot = partition(arr, start, end)</pre>
            <pre>   quickSort(arr, start, pivot-1)</pre>
            <pre>   quickSort(arr, pivot+1, end)</pre>
        </div>
    ),
    performance: {
        worstCase: (<span>O(n<sup>2</sup>)</span>),
        avgCase: (<span>O(nlog(n))</span>),
        bestCase: (<span>O(nlog(n))</span>),
        spaceComplexity: (<span>O(log(n))</span>)
    }
}


export default function QuickSort (array, length){
    let steps = newSteps(array);
    let temp;
    let instruction = ""
    function partition(arr, start, end) {
        let i = start + 1;
        let j = start + 1;

        instruction = `mark first element as pivot`;
        addStep(steps, arr, steps[steps.length - 1].sorted, instruction, [start]);
        
        while (j <= end) {
            instruction = `Comparing ${arr[j]} with pivot i.e ${arr[start]}`;
            addStep(steps, arr, steps[steps.length-1].sorted, instruction, [start, j], [], [], [...Array(i-(start+1)).keys()].map((k) => k + (start+1)));
            if (arr[j] < arr[start]) {
                instruction = `${arr[j]} < ${arr[start]} i.e. pivot so move it to the lesser(elements less than pivot) list (blue color)`;
                addStep(steps, arr, steps[steps.length-1].sorted, instruction, [start], [j], [], [...Array(i-(start+1)).keys()].map((k) => k + (start+1)));
                
                temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;

                addStep(steps, arr, steps[steps.length-1].sorted, instruction, [start], [i], [], [...Array(i-(start+1)).keys()].map((k) => k + (start+1)));
                i += 1;
            }
            else {
                instruction = `${arr[j]} >= ${arr[start]} i.e. pivot so continue`;
                addStep(steps, arr, steps[steps.length-1].sorted, instruction, [start], [], [], [...Array(i-(start+1)).keys()].map((k) => k + (start+1)));
            }
            j += 1;
        }

        instruction = `swap the last element of lesser list with pivot`;
        addStep(steps, arr, steps[steps.length-1].sorted, instruction, [i-1], [], [], [...Array((i-1) - start).keys()].map((k) => k + start));
        
        temp = arr[start];
        arr[start] = arr[i-1];
        arr[i-1] = temp;

        addStep(steps, arr, steps[steps.length-1].sorted, instruction, [i-1], [], [], [...Array((i-1) - start).keys()].map((k) => k + start));
        
        return i - 1;
    };

    function quickSort(arr, start, end) {
        if (start > end) {
            return null;
        }
        if (start === end) {
            instruction = `Only one element so mark it sorted`;
            addStep(steps, arr, [...steps[steps.length-1].sorted, start], instruction);
            return null;
        }

        let pivot = 0;

        pivot = partition(arr, start, end);

        instruction = `${arr[pivot]} is now sorted`;
        addStep(steps, arr, [...steps[steps.length-1].sorted, pivot], instruction);

        if (pivot > start) { 
            instruction = `Call quickSort for these elements`;
            addStep(steps, arr, steps[steps.length-1].sorted, instruction, [...Array((pivot) - start).keys()].map((k) => start + k));
            quickSort(arr, start, pivot-1);
        }
        if (pivot < end){    
            instruction = `Call quickSort for these elements`;
            addStep(steps, arr, steps[steps.length-1].sorted, instruction, [...Array(end - (pivot)).keys()].map((k) => (pivot+1) + k));
            quickSort(arr, pivot+1, end);
        }        

    }

    quickSort(array, 0, length - 1);
    instruction = `Array is sorted`;
    addStep(steps, array, steps[steps.length-1].sorted, instruction);
    return steps;
};