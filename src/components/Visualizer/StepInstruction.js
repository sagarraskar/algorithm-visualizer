export default function StepInstruction(props) {
    return (
        <div className={props.classType + " instruction"}>
            {props.instruction}
        </div>
    )
}