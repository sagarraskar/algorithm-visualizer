export default function Help() {
    return (
        <div>
            <div className="navbar">
                <div className="nav-brand">
                    <a href="/">Algorithm Visualizer</a>
                </div>
            </div>
            <div class="help">
                <div className="img-container">
                   <a href="/images/help.jpg" target="_blank"><img src="/images/help.jpg"></img></a>
                </div>
                <div className="instructions">
                <p><b>1. Select Algorithm:</b> Select the algorithm you want to visualize from the given list.</p>
                <p><b>2. Select Array Size:</b> Select the size of the array from the give list.</p>
                <p><b>3. Generate Random Array:</b> Generates random arra which will be visualized in the visualization area.</p>
                <p><b>4. Visualization Area:</b> This is the place where you will see the visualization. The vertical bars represent the elements in the array.</p>
                <p><b>5. Repeat:</b> It resets the array and run the visualization again from the start on the same array.</p>
                <p><b>6. Play/Pause</b> It is use to play or pause the visualization.</p>
                <p><b>7. Explanation:</b> It displays the statement which describes the current step which is being visualize.</p>
                <p><b>8. Speed:</b> It is used to change the speed of the visualization.</p>
                <p><b>9. Algorithm:</b>Displays the algorithm which is being visualize.</p>
                <p><b>10. Performance:</b>Displays the performance of the selected algorithm.</p>
                </div>
            </div>
        </div>
    );
}