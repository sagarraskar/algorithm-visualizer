import Algorithm from './Algorithm';
import Performance from './Performance';

export default function AlgorithmInfo(props) {
    return(
        <div className="algorithm_info">
            <Algorithm
                algorithm={props.desc.algorithm}
                name={props.desc.name}
            />
            <Performance
                performance={props.desc.performance}
            />
        </div>
    );
}