import { useState, useEffect } from 'react';
import SortBars from './SortBars';
import AlgorithmInfo from '../AlgorithmInfo/AlgorithmInfo';
import StepInstruction from './StepInstruction';
import Controls from './Controls';

export default function Visualizer(props) {
    const [array, setArray] = useState([]);
    const [originalArray, setOriginalArray] = useState([]);

    const [steps, setSteps] = useState([]);
    const [stepno, setStepno] = useState(-1);
    const [groupA, setGroupA] = useState([]);
    const [groupB, setGroupB] = useState([]);
    const [groupC, setGroupC] = useState([]);
    const [groupD, setGroupD] = useState([]);
    const [sorted, setSorted] = useState([]);
    const [instruction, setInstruction] = useState("");
    const [timeoutIds, setTimeoutIds] = useState([]);

    const [speed, setSpeed] = useState(0.25);
    const [running, setRunning] = useState(false);
    const [isrunning, setIsrunning] = useState(false);

    useEffect(() => {
        reset(props.array);
    }, [props.array]);

    useEffect(() => {
        clearTimeouts();
        setSteps(props.steps);
    }, [props.steps]);

    useEffect(() => {
        if(isrunning)
            play();
    }, [speed]);

    const reset = (array) => {
        setArray(array);
        setSteps([]);
        setStepno(-1);
        setGroupA([]);
        setGroupB([]);
        setGroupC([]);
        setGroupD([]);
        setSorted([]);
        setInstruction("");
        setOriginalArray([...array]);
        setRunning(false);
    }

    const changeState = (state) => {
        setArray(state.array);
        setGroupA(state.groupA);
        setGroupB(state.groupB);
        setGroupC(state.groupC);
        setGroupD(state.groupD);
        setSorted(state.sorted);
        setInstruction(state.instruction);
    }

    const clearTimeouts = () => {
        timeoutIds.forEach((timeoutId) => clearTimeout(timeoutId));
        setTimeoutIds([]);
    }

    const run = (step) => {
        const TimeoutIds = [];
        const timer = 350 / speed;
        setRunning(true);
        step.forEach((item, i) => {
            let timeoutId = setTimeout ((item) => {
                setStepno((prevstepno) => prevstepno + 1);
                changeState(item);
            }, i * timer, item);
            TimeoutIds.push(timeoutId);
        });
        
        let timeoutId = setTimeout(clearTimeouts, step.length * timer);
        TimeoutIds.push(timeoutId);
        setTimeoutIds(TimeoutIds);
    }

    const pause = () => {
        setRunning(false);
        clearTimeouts();
    }

    const play = () => {
        setRunning(true);
        const step = steps.slice(stepno);
        run(step);
    }
    const onSpeedChange = (value) => {
        setIsrunning(timeoutIds.length > 0);
        pause();
        setSpeed(Number(value));
    }

    const repeat = () => {
        clearTimeouts();
        setArray(originalArray);
        setStepno(-1);
        setGroupA([]);
        setGroupB([]);
        setGroupC([]);
        setGroupD([]);
        setSorted([]);
        setInstruction("");
        run(steps);
    }
    
    return (
        <div className="Visualizer">
            <SortBars
                numbers={array}
                maxNum={Math.max(...array)}
                groupA={groupA}
                groupB={groupB}
                groupC={groupC}
                groupD={groupD}
                sorted={sorted}
            />
            <StepInstruction
                instruction={instruction}
            />
            <Controls
                onPlay={stepno === -1 ? () => run(steps) : play}
                onPause={pause}
                onSpeedChange={onSpeedChange}
                repeat={repeat}
                running={timeoutIds.length > 0}
                speed={speed}
                playDisabled={(stepno >= steps.length - 1 && stepno !== -1) || steps.length <= 0}
                running={running}
            />
            <AlgorithmInfo
                desc={props.desc}
            />
        </div>
    );
}