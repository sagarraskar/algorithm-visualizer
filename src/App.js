import './App.css';
import { BrowserRouter, Route } from 'react-router-dom';
import HomePage from './components/pages/Homepage';
import HelpPage from './components/pages/Helppage';

function App() {
  return (
    <BrowserRouter>
      <Route exact path="/" component={HomePage}/>
      <Route path="/help" component={HelpPage}/>
    </BrowserRouter>
  );
}

export default App;
