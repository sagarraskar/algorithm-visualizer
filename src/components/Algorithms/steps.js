export const newSteps = (array) => {
    return [
        {
            array: [...array],
            groupA: [],
            groupB: [],
            groupC: [],
            groupD: [],
            sorted: [],
            instruction: ""
        }
    ];
};

export const addStep = (steps, array, sorted = [], instruction="", groupA = [], groupB = [], groupC = [], groupD = []) => {
    steps.push({
        array: [...array],
        groupA: [...groupA],
        groupB: [...groupB],
        groupC: [...groupC],
        groupD: [...groupD],
        sorted: [...sorted],
        instruction: instruction
    });
};

