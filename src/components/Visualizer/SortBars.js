const getBars = (
  numbers,
  maxNum,
  groupA,
  groupB,
  groupC,
  groupD,
  sorted
) => {
  return numbers.map((num, i) => {
    let width = 100 / numbers.length;
    let height = (num / maxNum) * 100;
    let stateA = groupA.includes(i);
    let stateB = groupB.includes(i);
    let stateC = groupC.includes(i);
    let stateD = groupD.includes(i);
    let issorted = sorted.includes(i);

    let margin =
      i === numbers.length ? '0' : width > 3 ? '0.5rem' : '0.125rem';
    
    let classNames = 'Bar';
    if (issorted) classNames += ' sorted';
    if (stateD) classNames += ' stateD';
    else if (stateC) classNames += ' stateC';
    else if (stateB) classNames += ' stateB';
    else if (stateA) classNames += ' stateA';
    
    let style = {marginRight: `${margin}`, width: `${width}%`, height: `${height}%`};

    return (
        <div key={`${i}_${num}`} style={style} className={classNames}>
            <span className="Bar_val">{width>4?num:null}</span>
        </div>
    );
  });
};

export default function SortBars (props) {
  return (
    <div className="SortBars">
      {getBars(
        props.numbers,
        props.maxNum,
        props.groupA,
        props.groupB,
        props.groupC,
        props.groupD,
        props.sorted
      )}
    </div>
  );
};


