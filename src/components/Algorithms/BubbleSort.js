import { newSteps, addStep } from './/steps';

export const BubbleSortDesc =  {
    name: "Bubble Sort",
    algorithm: (
        <div>
            <pre>for (i = 0 to indexofLastElement-1)</pre>
            <pre>   for (j = 0 to indexofLastUnsortedElement-1)</pre>
            <pre>       if (array[j] > array[j+1])</pre>
            <pre>           swap(array[j], array[j+1])</pre>
        </div>
    ),
    performance: {
        worstCase: (<span>O(n<sup>2</sup>)</span>),
        avgCase: (<span>O(n<sup>2</sup>)</span>),
        bestCase: (<span>O(n)</span>),
        spaceComplexity: (<span>O(1)</span>)
    }
}


export default function BubbleSort (array, length){
    let steps = newSteps(array);
    let temp;
    let instruction = ""
    for(let i = 0; i < length-1;i++) {
        for(let j = 0;j < length-i-1; j++) {
            // comparing
            instruction = `Comparing ${array[j]} and ${array[j+1]}`
            addStep(steps, array, steps[steps.length - 1].sorted, instruction, [j, j + 1]);
            if(array[j] > array[j+1]){
                //swaping
                instruction = `${array[j]} is greater than ${array[j+1]} so swap ${array[j]} and ${array[j+1]}`
                addStep(steps, array, steps[steps.length - 1].sorted, instruction, [], [j, j + 1]);
                temp = array[j];
                array[j] = array[j+1];
                array[j+1] = temp;
                addStep(steps, array, steps[steps.length - 1].sorted, instruction, [], [j, j + 1]);
            }
        }
        // last element is sorted
        addStep(steps, array, [...steps[steps.length-1].sorted, array.length-1-i]);
    }
    addStep(steps, array, [...steps[steps.length-1].sorted, 0], "array is sorted");
    return steps;
};